# Project

Created CRUD operations of jsonAPI.
Java is used as programming language.
TestNG is used as testing framework.
Tests for Create, Read and Update operations can be found in CRU.java class.
Test for Delete operation can be found in Delete.java class.
